const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const root = document.querySelector("#root");

class CreateList {
  constructor(arr) {
    this.arr = arr;

    const bookList = document.createElement("ul");
    root.append(bookList);

    this.arr.forEach((value) => {
      let { name, author, price } = value;

      if ("name" in value && "author" in value && "price" in value) {
        let bookItem = document.createElement("li");
        bookItem.innerHTML = `author: ${author} <br> name: ${name} <br> price: ${price}`;
        bookList.append(bookItem);
      }

      let index = this.arr.indexOf(value) + 1;

      if (
        !Object.keys(value).includes("price") ||
        !Object.keys(value).includes("author") ||
        !Object.keys(value).includes("name")
      ) {
        try {
          throw new Error(
            `Sorry.This book at index ${index} has no some attributes`
          );
        } catch (error) {
          console.log(error.message);
        }
      }

      // Old version:
      // if (!Object.keys(value).includes("price")) {
      //   try {
      //     throw new Error(`Sorry.This book at index ${index} has no price`);
      //   } catch (error) {
      //     console.log(error.message);
      //   }
      // } else if (!Object.keys(value).includes("author")) {
      //   try {
      //     throw new Error(`Sorry.This book at index ${index} has no author`);
      //   } catch (error) {
      //     console.log(error.message);
      //   }
      // } else if (!Object.keys(value).includes("name")) {
      //   try {
      //     throw new Error(`Sorry.This book at index ${index} has no name`);
      //   } catch (error) {
      //     console.log(error.message);
      //   }
      // }
    });
  }
}

const list = new CreateList(books);
